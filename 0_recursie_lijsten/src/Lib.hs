module Lib
    ( ex1, ex2, ex3, ex4, ex5, ex6, ex7
    ) where

-- | Bereken som van lijst
ex1 :: [Int] -> Int
ex1 [] = 0
ex1 (x:xs)= x + ex1 xs

-- | Verhoog elk element met 1
ex2 :: [Int] -> [Int]
ex2 [] = []
ex2 (x:xs) = x + 1: ex2 xs

-- | Vermenigvuldig elk element met -1
ex3 :: [Int] -> [Int]
ex3 [] = []
ex3 (x:xs) = x * (-1): ex3 xs

-- | Plak twee lijsten aan elkaar
ex4 :: [Int] -> [Int] -> [Int]
ex4 [] m = m
ex4 (x:xs) m = x: ex4 xs(m)

-- | Tel elementen van twee lijsten paarsgewijs op
ex5 :: [Int] -> [Int] -> [Int]
ex5 [][] = []
ex5 (x:xs)(x2:xs2) = x+x2: ex5(xs)(xs2)

-- | Vermenigvuldig elementen van twee lijsten paarsgewijs
ex6 :: [Int] -> [Int] -> [Int]
ex6 [][] = []
ex6 (x:xs)(x2:xs2) = x*x2: ex6(xs)(xs2)

-- | Inwendig product berekenen
ex7 :: [Int] -> [Int] -> Int
ex7 a b = ex1(ex6 a b)
